//REACT
import React from 'react';
//CUSTOM COMPONENTS
import Router from './components/Router';
import { createRoot } from 'react-dom/client';

import './styles/normalize.scss';
import './styles/app.scss';



function App (){
    return <Router/>;
}

const container = document.getElementById('root');
const root = createRoot(container); // createRoot(container!) if you use TypeScript
root.render(<App/>);