import React, {createContext} from 'react';
import axios from 'axios';
import Swal from 'sweetalert2'

export const ViajerosContext = createContext();

class ViajerosContextProvider extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            viajeros: [],
            viajesDisponibles: [],
            message: {},
        };
        this.readViajeros();
        this.viajesDisponibles();

    }


    //read
    async readViajeros() {
        await axios.get('/api/viajeros/list')
            .then(response => {

                this.setState({
                    viajeros: response.data
                });
            })
    }

    async createViajeros(data){

        await axios.post('api/viajeros/create',data)
        .then(response => {
            this.readViajeros();
            Swal.fire('¡Creado con exito!', '', 'success')
        });

    }


    async editViajero(data){
        await axios.post(`api/viajeros/edit`,data)
        .then(response => {
            this.readViajeros();
            Swal.fire('Editado con exito!', '', 'success')
        });
    }

    async deleteViajero(id){

        Swal.fire({
            title: '¿Desea borrar esté viajero?',
            text: 'No podras recuperar los datos, si tiene vuelos asociados automaticamente será colocado en Disponible.',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Eliminar',
            cancelButtonText: `Cancelar`,
            confirmButtonColor: 'red'
          }).then(async (result) => {
            if (result.isConfirmed)
                await axios.post(`api/viajeros/delete/${id}`)
                .then(response => {
                    this.readViajeros();
                    Swal.fire('¡Borrado con exito!', '', 'success')
                });


            
          })
    }

    async viajesDisponibles(){
    await axios.get('api/viajes/list-disponible').then(response => {
            this.setState({
                viajesDisponibles: response.data,
            });
        })
        
    }


    async createViaje(data){

        await axios.post(`api/viajeros/reserver-viaje/${data.viajero}/${data.viaje}`,data).then(response => {
            Swal.fire('¡Viaje creado con exito!', '', 'success')
        })

    }


    render() {
        return (
            <ViajerosContext.Provider value={{
                viajeros: this.state.viajeros,
                viajesDisp: this.state.viajesDisponibles,
                viajesDisponibles: this.viajesDisponibles.bind(this),
                createViajero: this.createViajeros.bind(this),
                createViaje: this.createViaje.bind(this),
                editViajero: this.editViajero.bind(this),
                deleteViajero: this.deleteViajero.bind(this),
                }}>
                {this.props.children}
            </ViajerosContext.Provider>
        );
    }
}


export default ViajerosContextProvider;