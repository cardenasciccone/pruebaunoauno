import React, {createContext} from 'react';
import axios from 'axios';
import Swal from 'sweetalert2'


export const ViajesContext = createContext();

class ViajesContextProvider extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            viajes: [],
            message: {},
        };
        this.readViajes();
    }


    //read
    async readViajes() {
        await axios.get('/api/viajes/list')
            .then(response => {
                this.setState({
                    viajes: response.data,
                });
            })
    }

    async createViajes(data){
        await axios.post('api/viajes/create',data)
        .then(response => {
            Swal.fire('¡Viaje creado con exito!', '', 'success')
            this.readViajes();
        });
    }



    render() {
        return (
            <ViajesContext.Provider value={{
                viajes: this.state.viajes,
                createViajes: this.createViajes.bind(this)
                }}>
                {this.props.children}
            </ViajesContext.Provider>
        );
    }
}


export default ViajesContextProvider;