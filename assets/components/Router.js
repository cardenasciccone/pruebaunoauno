//REACT
import React from 'react';
//ROUTER
import {BrowserRouter,Route,Routes as Switch,Redirect} from 'react-router-dom';
//Components
import Navigator from "./Navigator";
import Viajeros from "./Viajeros";
import ViajerosContextProvider from "../context/ViajerosContext";

import Viajes from "./Viajes";
import ViajesContext from "../context/ViajesContext";

const AppViajeros = () => (
  <ViajerosContextProvider>
    <Viajeros/>
  </ViajerosContextProvider>
);


const AppViajes = () => (
  <ViajesContext>
      <Viajes/>
  </ViajesContext>

);


const Router = () => {
  return (
    <BrowserRouter>
      <Navigator/>
      <div id='content-div'> 

      <Switch>
        <Route path="/" element={<AppViajeros />} />
        <Route path="/viajeros" element={<AppViajeros />} />
        <Route path="/viajes" element={<AppViajes />} />

        
      </Switch>
      </div>
    </BrowserRouter>
  );
};

export default Router;
