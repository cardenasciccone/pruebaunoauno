import React, { Fragment, useContext, useState } from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import Button from "@mui/material/Button";
import DeleteIcon from "@mui/icons-material/Delete";
import RemoveRedEyeIcon from "@mui/icons-material/RemoveRedEye";
import EditIcon from "@mui/icons-material/Edit";
import { ViajesContext } from "../context/ViajesContext";
import AddIcon from "@mui/icons-material/Add";
import AirplaneTicketIcon from "@mui/icons-material/AirplaneTicket";
import Typography from "@mui/material/Typography";
import ModalViajes from "./ModalViajes";
import FlightTakeoffIcon from "@mui/icons-material/FlightTakeoff";


function Viajes() {
  const context = useContext(ViajesContext);
  const rows = context.viajes;

  const [modalShowCreate, setModalShowCreate] = React.useState(false);


  const sendForm = () => {
    // Get the form
    let form = document.querySelector("#viajesForm");

    // Get all field data from the form
    // returns a FormData object
    let data = new FormData(form);
    context.createViajes(data);

    setModalShowCreate(false);
  };



  const tableResult = function () {
    if (rows.length > 0)
      return rows.map((row) => (
        <TableRow
          key={row.id}
          sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
        >
          <TableCell align="left">{row.code}</TableCell>
          <TableCell align="left">{row.number_place}</TableCell>
          <TableCell align="left">{row.place_origin}</TableCell>
          <TableCell align="left">{row.destination}</TableCell>
          <TableCell align="left">{row.price} USD</TableCell>
          <TableCell aling="left" colSpan={2}>
            {row.viajes == "null" ? <p style={{color:"green"}}>Disponible</p> : <p style={{color:"red"}}>Reservado</p>}
          </TableCell>
        </TableRow>
      ));

    return (
      <TableRow>
        <TableCell colSpan="7" style={{ textAlign: "center" }}>
          No hay datos disponibles.
        </TableCell>
      </TableRow>
    );
  };

  return (
    <div>
      <Typography
        variant="h4"
        noWrap
        component="div"
      >
       <FlightTakeoffIcon/> Viajes
      </Typography>

      <Typography
        variant="small"
        component="div"
      >En está area podras crear y ver todos los viajes de la base de datos.</Typography>

      <div style={{padding:"20px"}} />

      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Cod. Viaje</TableCell>
              <TableCell align="left">Número de plazas</TableCell>
              <TableCell align="left">Destino</TableCell>
              <TableCell align="left">Lugar de origen</TableCell>
              <TableCell align="left">Precio</TableCell>
              <TableCell align="left">Status</TableCell>
              <TableCell align="right">
                <Button color="success" variant="contained" 
                onClick={() => setModalShowCreate(true)}>
                  <AddIcon />
                </Button>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>{tableResult()}</TableBody>
        </Table>
      </TableContainer>

      <ModalViajes
        show={modalShowCreate}
        onHide={() => setModalShowCreate(false)}
        formSend={() => sendForm()}
      />

    </div>
  );
}

export default Viajes;
