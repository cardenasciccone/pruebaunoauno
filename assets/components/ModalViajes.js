import * as React from "react";
import { Modal, Button,InputGroup, FormControl } from "react-bootstrap";
import SaveIcon from '@mui/icons-material/Save';
import TextField from '@mui/material/TextField'

function ModalViajes(props) {


  
    return (
        <Modal
          {...props}
    
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
                Viajes
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h4 className="mb-3">Completa la información del viaje</h4>
            <form id='viajesForm'>
    

              <InputGroup className="mb-3">
                <TextField name='code' type='Number' label="Cod. Viaje" color="primary" style={{width:"100%"}} />
              </InputGroup>
    
              <InputGroup className="mb-3">
                <TextField name='number_place' type='Number' label="Número de plaza" color="primary" style={{width:"100%"}} />
              </InputGroup>
        
              <InputGroup className="mb-3">
                <TextField name='place_origin' type='text' label="Origen" color="primary" style={{width:"100%"}} />
              </InputGroup>
    
              <InputGroup className="mb-3">
                <TextField name='destination' type='text' label="Destino" color="primary" style={{width:"100%"}} />
              </InputGroup>

              <InputGroup className="mb-3">
                <TextField name='price' type='Number' label="Precio (USD)" color="primary" style={{width:"100%"}} />
              </InputGroup>
    
            </form>
          </Modal.Body>
          <Modal.Footer>
            <Button className="px-3 py-2" onClick={() => props.formSend()}>Guardar <SaveIcon/></Button>
            </Modal.Footer>
        </Modal>
      );
}

export default ModalViajes;