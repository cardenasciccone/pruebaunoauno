import React, { Fragment, useContext, useState } from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Tooltip from "@mui/material/Tooltip";
import Paper from "@mui/material/Paper";
import Button from "@mui/material/Button";
import DeleteIcon from "@mui/icons-material/Delete";
import RemoveRedEyeIcon from "@mui/icons-material/RemoveRedEye";
import EditIcon from "@mui/icons-material/Edit";
import { ViajerosContext } from "../context/ViajerosContext";
import AddIcon from "@mui/icons-material/Add";
import AirplaneTicketIcon from "@mui/icons-material/AirplaneTicket";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import ModalAddViajero from "./ModalAddViajero";
import ModalTicket from "./ModalTicket";

import PersonIcon from "@mui/icons-material/Person";

function Viajeros() {
  const context = useContext(ViajerosContext);
  const rows = context.viajeros;

  const [modalShowCreate, setModalShowCreate] = React.useState(false);
  const [modalShowTicket, setModalShowTicket] = React.useState(false);

  const [viajero,setViajero] = React.useState([]);
  const [viajesDisponibles,setViajesDisponibles] = React.useState([]);

  const [show,setShow] = React.useState(false);


  const sendForm = () => {
  
    let form = document.querySelector("#viajerosForm");
    let data = new FormData(form);


    viajero.id != undefined ? context.editViajero(data) : context.createViajero(data);

    setModalShowCreate(false);
  };


 

  const sendTicketForm = (insert = false) => {

    if(insert == true){ 
        const data = {
            viajero: viajero.id,
            viaje: parseInt(document.querySelector("#formViaje input").value)
        }
        context.createViaje(data);
        context.viajesDisponibles();
    }

    setModalShowTicket(false);
  }


  const editViajero = (data) => {
    setModalShowCreate(true);
    setViajero(data);
    setShow(false);
  }

  const showViajero = (data) => {
    setModalShowCreate(true);
    setViajero(data);
    setShow(true);
  }

 
  const tableResult = function () {
    if (rows.length > 0)
      return rows.map((row) => (
        <TableRow
          key={row.id}
          sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
        >
          <TableCell component="th" scope="row">
            {row.identification}
          </TableCell>
          <TableCell align="left">{row.fullname}</TableCell>
          <TableCell align="left">{row.date}</TableCell>
          <TableCell align="left">{row.celphone}</TableCell>
          <TableCell align="right">
            <Tooltip title="Agregar viaje">
              <Button color="success" onClick={async () => {
                  setViajesDisponibles(context.viajesDisp);
                  setViajero(row);
                  setModalShowTicket(true)
                  }}>
                <AirplaneTicketIcon />
              </Button>
            </Tooltip>
            <Tooltip title="Ver detalle">
              <Button 
              onClick={() => showViajero(row)}
              color="primary">
                <RemoveRedEyeIcon />
              </Button>
            </Tooltip>
            <Tooltip title="Editar">
              <Button 
              color="warning"
              onClick={() => editViajero(row)}
              >
                <EditIcon />
              </Button>
            </Tooltip>
            <Tooltip title="Borrar">
              <Button
                onClick={() => context.deleteViajero(row.id)}
                color="error"
              >
                <DeleteIcon />
              </Button>
            </Tooltip>
          </TableCell>
        </TableRow>
      ));

    return (
      <TableRow>
        <TableCell colSpan="5" style={{ textAlign: "center" }}>
          No hay datos disponibles.
        </TableCell>
      </TableRow>
    );
  };

  return (
    <div>
      <Typography
        variant="h4"
        noWrap
        component="div"
      >
        <PersonIcon/>Viajeros
      </Typography>

      <Typography
        variant="small"
        component="div"
      >En está area podras crear,editar,eliminar,ver y crear tickets; a todos los viajeros de la base de datos.</Typography>

      <div style={{padding:"20px"}} />

      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Doc. Identificación</TableCell>
              <TableCell align="left">Nombre Completo</TableCell>
              <TableCell align="left">Fecha Nacimiento</TableCell>
              <TableCell align="left">Teléfono</TableCell>
              <TableCell align="right">
                <Tooltip title="Agregar viajero">
                  <Button
                    color="success"
                    variant="contained"
                    onClick={() => { 
                        setViajero([]);
                        setShow(false);
                        setModalShowCreate(true)
                    }}
                  >
                    <AddIcon />
                  </Button>
                </Tooltip>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>{tableResult()}</TableBody>
        </Table>
      </TableContainer>

      <ModalAddViajero
        show={modalShowCreate}
        onHide={() => setModalShowCreate(false)}
        formSend={() => sendForm()}
        viajero={viajero}
        showView={show}
      />
     <ModalTicket
        show={modalShowTicket}
        viajero={viajero}
        viajesDisponibles={viajesDisponibles}
        sendTicketForm={sendTicketForm}
      />
    </div>
  );
}

export default Viajeros;
