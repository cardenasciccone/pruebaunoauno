import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import InputLabel from '@mui/material/InputLabel';
import OutlinedInput from '@mui/material/OutlinedInput';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';


export default function ModalTicket(props) {



  const handleClose = (insert = false) => {
    props.sendTicketForm(insert);
  };

  return (
    <div>
      <Dialog disableEscapeKeyDown open={props.show} onClose={() => handleClose(false)}>
        <DialogTitle>Selecciona viaje</DialogTitle>
        <DialogContent>
          <Box id="formViaje" component="form" sx={{ display: 'flex', flexWrap: 'wrap' }}>
            <FormControl id="selectViaje" sx={{ m: 1, minWidth: 400 }}>
              <InputLabel id="demo-dialog-select-label">Viajes Disponibles</InputLabel>
              <Select
                labelId="demo-dialog-select-label"
                
                input={<OutlinedInput label="Viajes Disponibles" />}
              >
                <MenuItem value="">
                  <em>{props.viajesDisponibles.length ? "Selecciona viaje" : "- No hay viajes disponibles -"}</em>
                </MenuItem>
                {props.viajesDisponibles.map((row) => ( 
                    <MenuItem value={row.id} key={row.id}>{row.destination} hacia {row.place_origin}</MenuItem>
                ))}
              </Select>
            </FormControl>
          </Box>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => handleClose(false)}>Cancelar</Button>
          <Button onClick={() => handleClose(true)}>Aceptar</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}