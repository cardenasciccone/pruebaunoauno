import * as React from "react";
import { Modal, Button, InputGroup, FormControl } from "react-bootstrap";
import SaveIcon from "@mui/icons-material/Save";
import TextField from "@mui/material/TextField";

function ModalAddViajero(props) {
  const buttons = () => {
    if (!props.showView)
      return (
        <Modal.Footer>
          <Button className="px-3 py-2" onClick={() => props.formSend()}>
            Guardar <SaveIcon />
          </Button>
        </Modal.Footer>
      );

    return <></>;
  };

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">Viajero</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <h4 className="mb-3">
          {!props.showView
            ? "Completa la información del usuario"
            : "Información del usuario"}
        </h4>
        <form id="viajerosForm">
          <InputGroup className="mb-3">
            <FormControl
              name="id"
              id="idViajero"
              type="hidden"
              value={props.viajero.id}
            />
          </InputGroup>

          <InputGroup className="mb-3">
            <TextField
              name="identification"
              inputProps={{
                readOnly: props.showView,
                disabled: props.showView,
              }}
              defaultValue={props.viajero.identification}
              type="text"
              label="Documento identificación"
              color="primary"
              fullWidth
            />
          </InputGroup>

          <InputGroup className="mb-3">
            <TextField
              name="fullname"
              inputProps={{
                readOnly: props.showView,
                disabled: props.showView,
              }}
              defaultValue={props.viajero.fullname}
              type="text"
              label="Nombre completo"
              color="primary"
              fullWidth
            />
          </InputGroup>

          <InputGroup className="mb-3">
            <small>Fecha de nacimiento:</small>
            <TextField
              name="date"
              type="date"
              inputProps={{
                readOnly: props.showView,
                disabled: props.showView,
              }}
              defaultValue={props.viajero.date}
              color="primary"
              fullWidth
            />
          </InputGroup>

          <InputGroup className="mb-3">
            <TextField
              name="celphone"
              type="number"
              inputProps={{
                readOnly: props.showView,
                disabled: props.showView,
              }}
              defaultValue={props.viajero.celphone}
              label="Teléfono"
              color="primary"
              fullWidth
            />
          </InputGroup>
        </form>
      </Modal.Body>
      {buttons()}
    </Modal>
  );
}

export default ModalAddViajero;
