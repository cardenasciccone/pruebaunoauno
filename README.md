# SYMFONY-REACT (TODOAPP) PruebaUnoaUno


## Instalación

**1.- Clonar el proyecto de `https://gitlab.com/cardenasciccone/pruebaunoauno.git` ejecutar comando `git clone https://gitlab.com/cardenasciccone/pruebaunoauno.git`**

**2.- Instalar dependencias atraves de `composer install`, si no tiene la herramienta puede instalarla de `https://getcomposer.org`**

**3.- Colocar el repositorio en un servidor Apache, también se puede hacer uso de `symfony server:start ` o  `php -S localhost:8000 -t public `**

**4.- Editamos las credenciales de `.env` para crear la base de datos `DATABASE_URL="mysql://NOMBRE:PASSWORD@127.0.0.1:3306/db_appviajes?serverVersion=5.7&charset=utf8mb4`. Después ejecutamos el comando  `php bin/console doctrine:database:create` esto creara una base de datos en MySQL**

**5.- Crearemos los archivos de migracion `php bin/console make:migration`**

**6.- Subir archivos de migracion  `php bin/console doctrine:migrations:migrate`**

**7.- Para ejecutar React necesitamos el uso de `yarn` si no cuenta con la herramienta, puedes visitar la página de instalación `https://classic.yarnpkg.com/lang/en/docs/install/ `**

**8.- Cuando tengamos `yarn` ejecutamos en la consola dos comandos  `yarn install` y después `yarn encore dev --watch`**
*CONSEJO: Si queremos validar que está correctamente instalado ejecutar `yarn -v`)*

**9.- Ir al navegador y colocar la direccion `127.0.0.1:8000 o localhost:8000`**

**IMPORTANTE: DEBEMOS ESTAR EN LA CARPETA DEL REPOSITORIO, PARA EFECTUAR DICHOS PROCESOS**

## Uso de API

**API Viajes**

***Route("/api/viajes/list", methods={"GET","HEAD"}) response json(Array)***

***Route("api/viajes/list-disponible", methods={"GET","HEAD"}) response json (Array) devuelve los viajes disponibles***

***Route("api/viajes/create", methods={"GET","POST"}) send json({ code: CODE, number_place: NUMBER_PLACE,place_origin: PLACE_ORIGIN,price: PRICE,destination: DESTINATION }) response json (devuelve si la solitud es correcta)***

***Route("/api/viajes/edit/{id}", methods={"GET","POST"}) send json({ code: CODE, number_place: NUMBER_PLACE,place_origin: PLACE_ORIGIN,price: PRICE,destination: DESTINATION }) response json (devuelve si la solitud es correcta)***

***Route("/api/viajes/delete/{id}", methods={"GET","POST"}) response json (devuelve si la solitud es correcta)***

**API Viajeros**

***Route("/api/viajeros/list", methods={"GET","HEAD"}) response json(Array)***

***Route("/api/viajeros/create", methods={"POST","HEAD"}) send json({ identification: IDENTIFICATION,fullname: FULLNAME,date: DATE,celphone: CELPHONE }) response json (devuelve si la solitud es correcta)***

***Route("/api/viajeros/edit", methods={"POST","HEAD"}) send json({ id: ID, identification: IDENTIFICATION,fullname: FULLNAME,date: DATE,celphone: CELPHONE }) esponse json (devuelve si la solitud es correcta)***

***Route("api/viajeros/delete/{id}", methods={"POST","HEAD"}) response json (devuelve si la solitud es correcta)***


***Route("/api/viajeros/reserver-viaje/{viajero}/{viaje}", methods={"POST","HEAD"}) response json(devuelve si la solitud es correcta)***

***Route("/api/viajeros/get-viaje/{id_user}", methods={"GET","HEAD"}) response json(Array)***



## Imagenes de la prueba

![Alt text](imageReadme/prueba1.png?raw=true "Title")

![Alt text](imageReadme/prueba2.png?raw=true "Title")

## Licencia

MIT