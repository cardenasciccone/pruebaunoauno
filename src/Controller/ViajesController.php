<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Routing\Annotation\Route;
//Entity
use App\Entity\Viajeros;
use App\Entity\ViajesPivot;
use App\Entity\Viajes;

//Repository
use App\Repository\ViajesRepository;
use App\Repository\ViajesPivotRepository;

class ViajesController extends AbstractController
{

    private $entityManager;
    private $ViajesRepository;

    public function __construct(EntityManagerInterface $entityManager, ViajesRepository $ViajesRepository)
    {
        $this->entityManager = $entityManager;
        $this->ViajesRepository = $ViajesRepository;
    }
    

    /**
     * @Route("/api/viajes/list", methods={"GET","HEAD"})
     */
    public function list()
    {
        $viajes = $this->ViajesRepository->findAll();
        
        //map response
        $array = [];
        foreach ($viajes as $viaje) { $array[] = $viaje->toArray(); }

        
        return $this->json($array);
    }



    /**
     * @Route("/api/viajes/list-disponible", methods={"GET","HEAD"})
     */
    public function list_disponible()
    {
        $viajes = $this->ViajesRepository->findAll();
        
        //map response
        $array = [];
        foreach ($viajes as $viaje) { 
            $objeto = $viaje->toArray();
            if($objeto['viajes'] == "null") $array[] = $objeto; 
        }

        return $this->json($array);
    }


        /**
     * @Route("/api/viajes/create", methods={"POST","HEAD"})
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        try{ 
 
            $data = new Viajes();
            $data->setCode($request->get('code'));
            $data->setNumberPlace($request->get('number_place'));
            $data->setPlaceOrigin($request->get('place_origin'));
            $data->setPrice($request->get('price'));
            $data->setDestination($request->get('destination'));
    
            //add data in Database
            $this->entityManager->persist($data);
            $this->entityManager->flush();
            
            $response = new JsonResponse();
            $response->setData([
                'success' => 'true',
                'data' => 'null'
            ]);
    
            return $response;
    
        
            }catch(Exception $e){
    
            $response = new JsonResponse();
            $response->setData([
                  'success' => 'false',
                   'data' => $e
              ]);
                return $response;
            }
    }

    /**
     * @Route("/api/viajes/edit/{id}", methods={"POST","HEAD"})
     * @param Request $request
     * @return JsonResponse
     */
    public function edit(Request $request,int $id)
    {
        try{ 
    

            $data = $this->ViajesRepository->findOneBy(["id" => $id]);
    
            $data->setCode($request->get('code'));
            $data->setNumberPlace($request->get('number_place'));
            $data->setPlaceOrigin($request->get('place_origin'));
            $data->setPrice($request->get('price'));
            $data->setDestination($request->get('destination'));

    
            //edit DB 
            $this->entityManager->persist($data);
            $this->entityManager->flush();


            $response = new JsonResponse();
            $response->setData([
                'success' => 'true',
                'data' => 'null'
            ]);
    
            return $response;

    
           }catch(Exception $e){
    
             $response = new JsonResponse();
               $response->setData([
               'success' => 'false',
               'data' => $e
            ]);
            return $response;
            }
    



    }

    /**
     * @Route("/api/viajes/delete/{id}", methods={"POST","HEAD"})
     */
    public function delete(int $id)
    {

        $data = $this->ViajesRepository->findOneBy(["id" => $id]);

        //delete DB
        $this->entityManager->remove($data);
        $this->entityManager->flush();

        $response = new JsonResponse();
        $response->setData(['success' => 'true']);
        return $response;
    }


}
