<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Routing\Annotation\Route;
//Entity
use App\Entity\Viajeros;
use App\Entity\ViajesPivot;
use App\Entity\Viajes;

//Repository
use App\Repository\ViajerosRepository;
use App\Repository\ViajesPivotRepository;
use App\Repository\ViajesRepository;





class ViajerosController extends AbstractController
{

    private $entityManager;
    private $ViajerosRepository;
    private $ViajesPivotRepository;

    public function __construct(EntityManagerInterface $entityManager, ViajerosRepository $ViajerosRepository,ViajesPivotRepository $ViajesPivotRepository, ViajesRepository $ViajesRepository)
    {
        $this->entityManager = $entityManager;
        $this->ViajerosRepository = $ViajerosRepository;
        $this->ViajesPivotRepository = $ViajesPivotRepository;
        $this->ViajesRepository = $ViajesRepository;
    }
    

    /**
     * @Route("/api/viajeros/list", methods={"GET","HEAD"})
     */
    public function list()
    {

        $viajeros = $this->ViajerosRepository->findAll();
        
        //map response
        $array = [];
        foreach ($viajeros as $viajero) { $array[] = $viajero->toArray(); }

        return $this->json($array);
    }


    /**
     * @Route("/api/viajeros/create", methods={"POST","HEAD"})
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request)
    {   

        try{ 
 
        $data = new Viajeros();
        $data->setIdentification($request->get('identification'));
        $data->setFullname($request->get('fullname'));
        $data->setDate(\DateTime::createFromFormat('Y-m-d', $request->get('date')));
        $data->setCelphone($request->get('celphone'));

        //add data in Database
        $this->entityManager->persist($data);
        $this->entityManager->flush();
        
        $response = new JsonResponse();
        $response->setData([
            'success' => 'true',
            'data' => 'null'
        ]);

        return $response;

    
        }catch(Exception $e){

            $response = new JsonResponse();
            $response->setData([
               'success' => 'false',
                'data' => $e
           ]);
            return $response;
        }

    }

    /**
     * @Route("/api/viajeros/edit", methods={"POST","HEAD"})
     * @param Request $request
     * @return JsonResponse
     */
    public function edit(Request $request)
    {

        try{ 
    

        $data = $this->ViajerosRepository->findOneBy(["id" => $request->get('id')]);
        $data->setIdentification($request->get('identification'));
        $data->setFullname($request->get('fullname'));
        $data->setDate(\DateTime::createFromFormat('Y-m-d', $request->get('date')));
        $data->setCelphone($request->get('celphone'));
        
        //edit DB 
        $this->entityManager->persist($data);
        $this->entityManager->flush();

        $response = new JsonResponse();
        $response->setData([
            'success' => 'true',
            'data' => 'null'
         ]);
         return $response;

        }catch(Exception $e){

         $response = new JsonResponse();
            $response->setData([
           'success' => 'false',
            'data' => $e
        ]);
        return $response;
        }


    }

    /**
     * @Route("/api/viajeros/delete/{id}", methods={"POST","HEAD"})
     */
    public function delete(int $id)
    {




        $data = $this->ViajerosRepository->findOneBy(["id" => $id]);


        //remove pivots
        $pivots = $data->getViajesPivots();
        foreach($pivots as $pivot ){
            //delete DB
            $this->entityManager->remove($pivot);
            $this->entityManager->flush();    
        }


        //delete DB
        $this->entityManager->remove($data);
        $this->entityManager->flush();

        $response = new JsonResponse();
        $response->setData(['success' => 'true']);
        return $response;


    }

    /**
     * @Route("/api/viajeros/reserver-viaje/{viajero}/{viaje}", methods={"POST","HEAD"})
     * @return JsonResponse
     */
    public function reserverViaje(int $viajero,int $viaje){

        try{ 

        $data = new ViajesPivot();
        $data->setViajeros($this->ViajerosRepository->find($viajero));
        $data->setViajes($this->ViajesRepository->find($viaje));

        //add data in Database
        $this->entityManager->persist($data);
        $this->entityManager->flush();

                
        $response = new JsonResponse();
        $response->setData([
            'success' => 'true',
            'data' => "null"
        ]);

        return $response;

        }catch(Exception $e){
            $response = new JsonResponse();
            $response->setData([
             'success' => 'false',
                'data' => $e
           ]);
            return $response;
        }


    }

    /**
     * @Route("/api/viajeros/get-viaje/{id_user}", methods={"GET","HEAD"})
     */
    public function getViaje(int $id_user){
        
        $viajes = $this->ViajesPivotRepository->findBy(["id" => $id_user]);
        
        $array = [];
        foreach($viajes as $viaje){ 
            $array[] = $viaje->toArray(); 
        }

        $response = new JsonResponse();
        $response->setData([
            'success' => 'true',
            'data' => $array
        ]);
        return $response;

    }



}
