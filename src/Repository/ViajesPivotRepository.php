<?php

namespace App\Repository;

use App\Entity\ViajesPivot;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ViajesPivot>
 *
 * @method ViajesPivot|null find($id, $lockMode = null, $lockVersion = null)
 * @method ViajesPivot|null findOneBy(array $criteria, array $orderBy = null)
 * @method ViajesPivot[]    findAll()
 * @method ViajesPivot[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ViajesPivotRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ViajesPivot::class);
    }

    public function add(ViajesPivot $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(ViajesPivot $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return ViajesPivot[] Returns an array of ViajesPivot objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('v')
//            ->andWhere('v.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('v.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ViajesPivot
//    {
//        return $this->createQueryBuilder('v')
//            ->andWhere('v.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
