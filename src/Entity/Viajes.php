<?php

namespace App\Entity;

use App\Repository\ViajesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ViajesRepository::class)
 */
class Viajes
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $number_place;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $destination;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $place_origin;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\OneToOne(targetEntity=ViajesPivot::class, mappedBy="viajes", cascade={"persist", "remove"})
     */
    private $viajesPivot;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getNumberPlace(): ?string
    {
        return $this->number_place;
    }

    public function setNumberPlace(string $number_place): self
    {
        $this->number_place = $number_place;

        return $this;
    }

    public function getDestination(): ?string
    {
        return $this->destination;
    }

    public function setDestination(string $destination): self
    {
        $this->destination = $destination;

        return $this;
    }

    public function getPlaceOrigin(): ?string
    {
        return $this->place_origin;
    }

    public function setPlaceOrigin(string $place_origin): self
    {
        $this->place_origin = $place_origin;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getViajesPivot(): ?ViajesPivot
    {

        return $this->viajesPivot->toArray();
    }

    public function setViajesPivot(?ViajesPivot $viajesPivot): self
    {
        // unset the owning side of the relation if necessary
        if ($viajesPivot === null && $this->viajesPivot !== null) {
            $this->viajesPivot->setViajes(null);
        }

        // set the owning side of the relation if necessary
        if ($viajesPivot !== null && $viajesPivot->getViajes() !== $this) {
            $viajesPivot->setViajes($this);
        }

        $this->viajesPivot = $viajesPivot;

        return $this;
    }

    public function toArray()
    {
        return ['id' => $this->id, 'code' => $this->code, 'number_place' => $this->number_place, 'destination' => $this->destination, 'place_origin' => $this->place_origin,'price' => $this->price,'viajes' => json_encode($this->viajesPivot)];
    }

}
