<?php

namespace App\Entity;

use App\Repository\ViajerosRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ViajerosRepository::class)
 */
class Viajeros
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $identification;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fullname;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $celphone;

    /**
     * @ORM\OneToMany(targetEntity=ViajesPivot::class, mappedBy="viajeros")
     */
    private $viajesPivots;

    public function __construct()
    {
        $this->viajesPivots = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdentification(): ?string
    {
        return $this->identification;
    }

    public function setIdentification(string $identification): self
    {
        $this->identification = $identification;

        return $this;
    }

    public function getFullname(): ?string
    {
        return $this->fullname;
    }

    public function setFullname(string $fullname): self
    {
        $this->fullname = $fullname;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        
        $this->date = $date;

        return $this;
    }

    public function getCelphone(): ?string
    {
        return $this->celphone;
    }

    public function setCelphone(string $celphone): self
    {
        $this->celphone = $celphone;

        return $this;
    }

    /**
     * @return Collection<int, ViajesPivot>
     */
    public function getViajesPivots(): Collection
    {
        return $this->viajesPivots;
    }

    public function addViajesPivot(ViajesPivot $viajesPivot): self
    {
        if (!$this->viajesPivots->contains($viajesPivot)) {
            $this->viajesPivots[] = $viajesPivot;
            $viajesPivot->setViajeros($this);
        }

        return $this;
    }

    public function removeViajesPivot(ViajesPivot $viajesPivot): self
    {
        if ($this->viajesPivots->removeElement($viajesPivot)) {
            // set the owning side to null (unless already changed)
            if ($viajesPivot->getViajeros() === $this) {
                $viajesPivot->setViajeros(null);
            }
        }

        return $this;
    }

    public function toArray()
    {




        return ['id' => $this->id, 'identification' => $this->identification, 'fullname' => $this->fullname, 'date' => $this->date->format("Y-m-d"), 'celphone' => $this->celphone];
    }

}
