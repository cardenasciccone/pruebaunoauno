<?php

namespace App\Entity;

use App\Repository\ViajesPivotRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ViajesPivotRepository::class)
 */
class ViajesPivot
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Viajeros::class, inversedBy="viajesPivots")
     */
    private $viajeros;

    /**
     * @ORM\OneToOne(targetEntity=Viajes::class, inversedBy="viajesPivot", cascade={"persist", "remove"})
     */
    private $viajes;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getViajeros(): ?Viajeros
    {
        return $this->viajeros;
    }

    public function setViajeros(?Viajeros $viajeros): self
    {
        $this->viajeros = $viajeros;

        return $this;
    }

    public function getViajes(): ?Viajes
    {
        return $this->viajes;
    }

    public function setViajes(?Viajes $viajes): self
    {
        $this->viajes = $viajes;

        return $this;
    }


    public function toArray()
    {
        return ['id' => $this->id, 'viajeros' => $this->viajeros->toArray(), 'viajes' => $this->viajes->toArray()];
    }
}
