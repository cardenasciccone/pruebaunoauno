<?php

namespace App\Form;

use App\Entity\Viajeros;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ViajerosForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('identification', TextType::class, [
                'constraints' => [
                    new NotBlank(['message' => 'Por favor, introduzca el campo Identificación']),
                    new Length([
                        'min' => 5,
                        'max' => 20,
                        'minMessage' => 'Minimo 5 caracteres',
                        'maxMessage' => 'Colocaste {{ value }} y el limite es de {{ limit }} caracteres!',
                    ])
                ]
            ])
            ->add('fullname', TextType::class, [
                'constraints' => [
                    new NotBlank(['message' => 'Por favor, introduzca el campo Identificación']),
                    new Length([
                        'min' => 5,
                        'max' => 20,
                        'minMessage' => 'Minimo 5 caracteres',
                        'maxMessage' => 'Colocaste {{ value }} y el limite es de {{ limit }} caracteres!',
                    ])
                ]
            ])
            ->add('celphone', TextType::class, [
                'constraints' => [
                    new NotBlank(['message' => 'Por favor, introduzca el campo Telefono']),
                    new Length([
                        'min' => 5,
                        'max' => 20,
                        'minMessage' => 'Minimo 5 caracteres',
                        'maxMessage' => 'Colocaste {{ value }} y el limite es de {{ limit }} caracteres!',
                    ])
                ]
            ])


        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Viajeros::class,
            'csrf_protection' => false,
        ]);
    }
}
