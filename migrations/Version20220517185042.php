<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220517185042 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE viajeros (id INT AUTO_INCREMENT NOT NULL, identification VARCHAR(255) NOT NULL, fullname VARCHAR(255) NOT NULL, date DATE NOT NULL, celphone VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE viajes (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(255) NOT NULL, number_place VARCHAR(255) NOT NULL, destination VARCHAR(255) NOT NULL, place_origin VARCHAR(255) NOT NULL, price DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE viajes_pivot (id INT AUTO_INCREMENT NOT NULL, viajeros_id INT DEFAULT NULL, viajes_id INT DEFAULT NULL, INDEX IDX_8E8D7E103E5D1336 (viajeros_id), UNIQUE INDEX UNIQ_8E8D7E109FE29561 (viajes_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE viajes_pivot ADD CONSTRAINT FK_8E8D7E103E5D1336 FOREIGN KEY (viajeros_id) REFERENCES viajeros (id)');
        $this->addSql('ALTER TABLE viajes_pivot ADD CONSTRAINT FK_8E8D7E109FE29561 FOREIGN KEY (viajes_id) REFERENCES viajes (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE viajes_pivot DROP FOREIGN KEY FK_8E8D7E103E5D1336');
        $this->addSql('ALTER TABLE viajes_pivot DROP FOREIGN KEY FK_8E8D7E109FE29561');
        $this->addSql('DROP TABLE viajeros');
        $this->addSql('DROP TABLE viajes');
        $this->addSql('DROP TABLE viajes_pivot');
    }
}
